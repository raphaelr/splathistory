--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'SQL_ASCII';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: flag; Type: TABLE; Schema: public; Owner: splathistory; Tablespace:
--

CREATE TABLE flag (
    oid integer NOT NULL,
    date_event timestamp without time zone,
    title character varying,
    text character varying,
    color character varying,
    map integer
);


ALTER TABLE flag OWNER TO splathistory;

--
-- Name: flag_oid_seq; Type: SEQUENCE; Schema: public; Owner: splathistory
--

CREATE SEQUENCE flag_oid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE flag_oid_seq OWNER TO splathistory;

--
-- Name: flag_oid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: splathistory
--

ALTER SEQUENCE flag_oid_seq OWNED BY flag.oid;


--
-- Name: rotation; Type: TABLE; Schema: public; Owner: splathistory; Tablespace:
--

CREATE TABLE rotation (
    oid bigint,
    date_from timestamp without time zone,
    date_to timestamp without time zone,
    ranked_mode integer,
    ranked_maps integer[],
    regular_maps integer[],
    is_external boolean
);


ALTER TABLE rotation OWNER TO splathistory;

--
-- Name: rotation_oid_seq; Type: SEQUENCE; Schema: public; Owner: splathistory
--

CREATE SEQUENCE rotation_oid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE rotation_oid_seq OWNER TO splathistory;

--
-- Name: rotation_oid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: splathistory
--

ALTER SEQUENCE rotation_oid_seq OWNED BY rotation.oid;


--
-- Name: oid; Type: DEFAULT; Schema: public; Owner: splathistory
--

ALTER TABLE ONLY flag ALTER COLUMN oid SET DEFAULT nextval('flag_oid_seq'::regclass);


--
-- Name: oid; Type: DEFAULT; Schema: public; Owner: splathistory
--

ALTER TABLE ONLY rotation ALTER COLUMN oid SET DEFAULT nextval('rotation_oid_seq'::regclass);


--
-- Name: flag_pkey; Type: CONSTRAINT; Schema: public; Owner: splathistory; Tablespace:
--

ALTER TABLE ONLY flag
    ADD CONSTRAINT flag_pkey PRIMARY KEY (oid);

--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--
