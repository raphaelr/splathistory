﻿namespace SplatHistory.Model
{
    public enum RankedMode
    {
        SplatZones = 0,
        Rainmaker = 1,
        TowerControl = 2
    }
}