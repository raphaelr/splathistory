﻿using System.Collections.Immutable;

namespace SplatHistory.Model
{
    public class GameMapData
    {
        public GameMap Id { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public int Order { get; set; }

        private GameMapData(GameMap id, string name, string shortName, int order)
        {
            Id = id;
            Name = name;
            ShortName = shortName;
            Order = order;
        }

        public static readonly ImmutableDictionary<GameMap, GameMapData> Data = new[]
        {
            new GameMapData(GameMap.AnchoV, "Ancho-V Games", "AV", 0),
            new GameMapData(GameMap.Mall, "Arowana Mall", "AM", 1),
            new GameMapData(GameMap.Skatepark, "Blackbelly Skatepark", "BS", 2),
            new GameMapData(GameMap.Depot, "Bluefin Depot", "BD", 3),
            new GameMapData(GameMap.Camp, "Camp Triggerfish", "CT", 4),
            new GameMapData(GameMap.Flounder, "Flounder Heights", "FH", 5),
            new GameMapData(GameMap.Bridge, "Hammerhead Bridge", "HB", 6),
            new GameMapData(GameMap.Dome, "Kelp Dome", "KD", 7),
            new GameMapData(GameMap.MahiMahi, "Mahi-Mahi Resort", "MM", 8),
            new GameMapData(GameMap.Towers, "Moray Towers", "MT", 9),
            new GameMapData(GameMap.Museum, "Museum d'Alfonsino", "MA", 10),
            new GameMapData(GameMap.Pit, "Piranha Pit", "PP", 11),
            new GameMapData(GameMap.Port, "Port Mackerel", "PM", 12),
            new GameMapData(GameMap.Saltspray, "Saltspray Rig", "SR", 13),
            new GameMapData(GameMap.Underpass, "Urchin Underpass", "UU", 14),
            new GameMapData(GameMap.Warehouse, "Walleye Warehouse", "WW", 15)
        }.ToImmutableDictionary(x => x.Id);
    }
}