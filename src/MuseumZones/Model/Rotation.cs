﻿using System;
using System.ComponentModel;
using System.Linq;
using NPoco;

namespace SplatHistory.Model
{
    [TableName("rotation"), PrimaryKey("oid", AutoIncrement = true)]
    public class Rotation
    {
        [Column("oid")]
        public long ObjectId { get; set; }
        [Column("date_from")]
        public DateTime DateFrom { get; set; }
        [Column("date_to")]
        public DateTime DateTo { get; set; }
        [Column("ranked_mode")]
        public RankedMode RankedMode { get; set; }
        [Ignore]
        public GameMap[] RankedMaps { get; set; }
        [Ignore]
        public GameMap[] RegularMaps { get; set; }

        [Column("ranked_maps")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public int[] RankedMapsDbVal
        {
            get { return RankedMaps.Cast<int>().ToArray(); }
            set { RankedMaps = value.Select(x => (GameMap) x).ToArray(); }
        }

        [Column("regular_maps")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public int[] RegularMapsDbVal
        {
            get { return RegularMaps.Cast<int>().ToArray(); }
            set { RegularMaps = value.Select(x => (GameMap)x).ToArray(); }
        }
    }
}