﻿using System;
using System.ComponentModel;
using NPoco;

namespace SplatHistory.Model
{
    [TableName("flag"), PrimaryKey("oid", AutoIncrement = true)]
    public class Flag
    {
        [Column("oid")]
        public int Oid { get; set; }
        [Column("date_event")]
        public DateTime DateEvent { get; set; }
        [Column("title")]
        public string Title { get; set; }
        [Column("text")]
        public string Text { get; set; }
        [Column("color")]
        public string Color { get; set; }
        [Ignore]
        public GameMap? Map { get; set; }

        [Column("map")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public int? MapDbVal
        {
            get { return (int?) Map; }
            set { Map = (GameMap?) value; }
        }
    }
}