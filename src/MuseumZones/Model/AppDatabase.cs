﻿using System;
using System.Data;
using System.Data.Common;
using Microsoft.Extensions.Logging;
using NPoco;

namespace SplatHistory.Model
{
    public class AppDatabase : Database
    {
        private readonly ILogger _logger;

        public AppDatabase(ILoggerFactory loggerFactory, string connectionString, DatabaseType databaseType,
            DbProviderFactory provider, IsolationLevel? isolationLevel = null, bool enableAutoSelect = true)
            : base(connectionString, databaseType, provider, isolationLevel, enableAutoSelect)
        {
            _logger = loggerFactory.CreateLogger(typeof (AppDatabase));
        }

        protected override void OnException(Exception exception)
        {
            base.OnException(exception);
            if (_logger.IsEnabled(LogLevel.Error))
            {
                _logger.LogError(1, exception, "Last command: {1}", LastCommand);
            }
        }

        protected override void OnExecutedCommand(DbCommand cmd)
        {
            base.OnExecutedCommand(cmd);
            if (_logger.IsEnabled(LogLevel.Debug))
            {
                _logger.LogDebug(2, "{0}", LastCommand);
            }
        }
    }
}