﻿namespace SplatHistory.Model.ViewModels
{
    public class FlagViewModel
    {
        public long DateEvent { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public string Color { get; set; }
        public GameMap? Map { get; set; }

        public FlagViewModel(Flag flag)
        {
            DateEvent = (long) (flag.DateEvent - RotationViewModel.UnixEpoch).TotalMilliseconds;
            Title = flag.Title;
            Text = flag.Text;
            Color = flag.Color;
            Map = flag.Map;
        }
    }
}