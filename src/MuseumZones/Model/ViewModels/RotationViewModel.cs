﻿using System;

namespace SplatHistory.Model.ViewModels
{
    public class RotationViewModel
    {
        public static readonly DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public long DateFrom { get; set; }
        public long DateTo { get; set; }
        public GameMap[] RankedMaps { get; set; }
        public GameMap[] RegularMaps { get; set; }
        public RankedMode RankedMode { get; set; }

        public RotationViewModel(Rotation rot)
        {
            DateFrom = (long) (rot.DateFrom - UnixEpoch).TotalMilliseconds;
            DateTo = (long) (rot.DateTo - UnixEpoch).TotalMilliseconds;
            RankedMaps = rot.RankedMaps;
            RegularMaps = rot.RegularMaps;
            RankedMode = rot.RankedMode;
        }
    }
}