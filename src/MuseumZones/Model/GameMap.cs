﻿namespace SplatHistory.Model
{
    public enum GameMap
    {
        Underpass = 0,
        Warehouse = 1,
        Saltspray = 2,
        Mall = 3,
        Skatepark = 4,
        Port = 5,
        Dome = 6,
        Depot = 7,
        Towers = 8,
        Camp = 9,
        Flounder = 10,
        Bridge = 11,
        Museum = 12,
        MahiMahi = 13,
        Pit = 14,
        AnchoV = 15
    }
}