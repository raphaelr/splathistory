﻿var webpack = require("webpack");
var ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
    entry: {
        app: "./client/js/main",
        styles: ["./client/css/reset.css", "./client/css/main.css"],
        vendor: [
            "./node_modules/jquery/dist/jquery",
            "./node_modules/highstock-release/highstock",
            "./node_modules/highstock-release/highcharts-more",
            "./node_modules/highstock-release/modules/exporting"
        ]
    },

    output: {
        filename: "[name].js",
        path: __dirname + "/wwwroot/build",
        publicPath: "/build/"
    },

    devtool: "source-map",

    resolve: {
        extensions: [".js", ".css", ".json"]
    },

    module: {
        rules: [
            { test: /\.css$/, use: ExtractTextPlugin.extract({ fallback: "style-loader", use: "css-loader" }) },
            { test: /\.woff$/, use: "file-loader" }
        ]
    },

    plugins: [
        new webpack.optimize.CommonsChunkPlugin({ name: "vendor", filename: "vendor.js" }),
        new ExtractTextPlugin("styles.css")
    ],

    performance: {
        hints: false
    }
};

