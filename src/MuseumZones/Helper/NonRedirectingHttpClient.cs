﻿using System.Net.Http;

namespace SplatHistory.Helper
{
    public class NonRedirectingHttpClient
    {
        public HttpClient Value { get; }

        public NonRedirectingHttpClient()
        {
            var handler = new HttpClientHandler {AllowAutoRedirect = false};
            Value = new HttpClient(handler);
        }
    }
}