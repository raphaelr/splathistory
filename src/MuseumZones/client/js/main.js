﻿var $ = require("jquery");
var Highcharts = require("highstock-release/highstock");
require("highstock-release/modules/exporting")(Highcharts);
require("highstock-release/highcharts-more")(Highcharts);

var mz = {
    timelineChart: null,
    statChart: null,
    mapDataHash: null,
    mapDataArray: null,
    currentData: {},
    oneDay: 1000 * 60 * 60 * 24,
    globalEnd: Date.UTC(2017, 9, 4),

    convertToTimelineSeries: function(rotations) {
        var series = Array.apply(null, Array(mz.mapDataArray.length + 3)).map(function () { return []; });
        rotations.forEach(function (rotation) {
            var rotationSeries = series[rotation.rankedMode];
            this.addTimeRange(rotationSeries, rotation.dateFrom, rotation.dateTo, 1, 2);

            rotation.rankedMaps.forEach(function (map) {
                var mapData = this.mapDataHash[map];
                var floor = mapData.inverseOrder;
                var ceiling = mapData.inverseOrder + 1;
                var currentSeries = series[3 + mapData.order];
                this.addTimeRange(currentSeries, rotation.dateFrom, rotation.dateTo, floor, ceiling);
            }, this);
        }, this);
        return series;
    },

    addTimeRange: function(series, xFrom, xTo, floor, ceiling) {
        var interval = 1000 * 60 * 60;

        for (var x = xFrom; x < xTo; x += interval) {
            series.push([x, floor, ceiling]);
        }
        series.push([xTo - 1, floor, ceiling]);
        series.push([xTo, null, null]);
    },

    convertToStatSeries: function (rotations) {
        var series = [0,0,0,0].map(function() {
            return mz.mapDataArray.map(function () { return 0; });
        });
        rotations.forEach(function (rotation) {
            rotation.rankedMaps.forEach(function (map) {
                var mapData = this.mapDataHash[map];
                var currentSeries = series[rotation.rankedMode];
                currentSeries[mapData.order]++;
            }, this);
            rotation.regularMaps.forEach(function (map) {
                var mapData = this.mapDataHash[map];
                series[3][mapData.order]++;
            }, this);
        }, this);
        return series;
    },

    loadChartData: function (dateFrom, dateTo) {
        var timeline = this.timelineChart;
        var stat = this.statChart;
        timeline.showLoading("Loading...");

        var successHandler = function (data) {
            mz.currentData = { from: new Date(dateFrom), to: new Date(dateTo), data: data };

            var timelineSeries = mz.convertToTimelineSeries(data);
            timelineSeries.forEach(function (s, i) {
                timeline.series[i].setData(s);
            });

            var statSeries = mz.convertToStatSeries(data);
            statSeries.forEach(function(s, i) {
                stat.series[i].setData(s);
            });
        };

        $.get("/data/history", { ticksFrom: dateFrom, ticksTo: dateTo }, successHandler, "json")
            .always(function() {
                timeline.hideLoading();
            });
    },

    checkChartReady: function () {
        if (this.timelineChart && this.statChart) {
            mz.loadChartData(mz.globalEnd - this.oneDay * 30, mz.globalEnd + this.oneDay);
        }
    },

    downloadTimelineCsv: function () {
        var header = ["From", "To", "Ranked Mode", "Ranked Map 1", "Ranked Map 2", "Turf War Map 1", "Turf War Map 2"];
        var rankedModes = ["Splat Zones", "Rainmaker", "Tower Control"];
        var data = this.currentData.data.map(function(rec) {
            return [
                new Date(rec.dateFrom).toISOString(),
                new Date(rec.dateTo).toISOString(),
                rankedModes[rec.rankedMode],
                this.getMapName(rec.rankedMaps[0]),
                this.getMapName(rec.rankedMaps[1]),
                this.getMapName(rec.regularMaps[0]),
                this.getMapName(rec.regularMaps[1])
            ];
        }, this);
        this.downloadCsv("SplatoonTimeline.csv", header, data);
    },

    getMapName:function(id) {
        var mapData = this.mapDataHash[id];
        return mapData ? mapData.name : "";
    },

    downloadStatCsv: function() {
        var header = ["From", "To", "Map", "Splat Zones", "Rainmaker", "Tower Control", "Turf War"];
        var dateFrom = this.currentData.from.toISOString();
        var dateTo = this.currentData.to.toISOString();
        var data = [];
        this.mapDataArray.forEach(function(mapData) {
            data[mapData.id] = [dateFrom, dateTo, mapData.name, 0, 0, 0, 0];
        });
        this.currentData.data.forEach(function(rec) {
            rec.rankedMaps.forEach(function(map) {
                data[map][3 + rec.rankedMode]++;
            });
            rec.regularMaps.forEach(function(map) {
                data[map][6]++;
            });
        });
        data.sort(function (a, b) {
            a = a[2];
            b = b[2];
            return a < b ? -1 : a > b ? 1 : 0;
        });
        return this.downloadCsv("SplatoonMapStats.csv", header, data);
    },

    downloadCsv: function (name, header, data) {
        data.unshift(header);
        var dataUrl = "data:text/csv;base64," + btoa(data.map(function(rec) {
            return rec.map(function(field) {
                return '"' + String(field).replace('"', '""') + '"';
            }).join(",");
        }).join("\n"));

        var link = document.createElement("a");
        link.download = name;
        link.href = dataUrl;

        if (typeof MouseEvent === "function") {
            var evt = new MouseEvent("click", {
                view: window,
                bubbles: true,
                cancelable: true
            });
            link.dispatchEvent(evt);
        } else {
            alert("Please use a real browser to use this feature");
        }
    }
};

$(function () {
    var staticDataText = $("#staticdata").text();
    if (!staticDataText) return;

    var staticData = JSON.parse(staticDataText);
    mz.mapDataArray = staticData.mapData;
    mz.mapDataHash = {};
    mz.mapDataArray.sort(function(a, b) {
        return a.order - b.order;
    });
    mz.mapDataArray.forEach(function (x) {
        x.inverseOrder = mz.mapDataArray.length - x.order;
        mz.mapDataHash[x.id] = x;
    });

    var dateFrom = Date.UTC(2015, 7, 8);
    var dateTo = mz.globalEnd;

    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });

    var titleStyle = { fontFamily: "Paintball" };

    Highcharts.stockChart("timeline-container", {
        chart: {
            type: "arearange",
            events: {
                load: function() {
                    mz.timelineChart = this;
                    mz.checkChartReady();
                }
            }
        },
        credits: {
            enabled: false
        },
        exporting: {
            buttons: {
                contextButton: {
                    menuItems: [{
                        text: "Download raw data (CSV)",
                        onclick: function() {
                            mz.downloadTimelineCsv();
                        }
                    }].concat(Highcharts.getOptions().exporting.buttons.contextButton.menuItems)
                }
            }
        },
        title: {
            text: "Ranked map history",
            style: titleStyle
        },
        rangeSelector: {
            selected: 0
        },
        navigator: {
            adaptToUpdatedData: false
        },
        scrollbar: {
            liveRedraw: false
        },
        series: ["Splat Zones", "Rainmaker", "Tower Control"].map(function(name) {
            return {
                name: name,
                data: [[dateFrom, null, null], [dateTo, null, null]],
                color: "transparent",
                dataGrouping: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: 'Mode: {series.name}<br />'
                }
            }
        }).concat(mz.mapDataArray.map(function (mapData) {
            return {
                name: mapData.name,
                id: String(mapData.id),
                color: mapData.order % 2 === 1 ? "#285EFF" : "#71B21C",
                data: [[dateFrom, null, null], [dateTo, null, null]],
                dataGrouping: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}<br />'
                }
            };
        })).concat(mz.mapDataArray.concat([{id: null}]).map(function (mapData) {
            return {
                type: "flags",
                onSeries: String(mapData.id),
                data: staticData.flags.filter(function(flag) {
                    return flag.map === mapData.id;
                }).map(function (flag) {
                    return {
                        x: flag.dateEvent,
                        title: flag.title,
                        text: flag.text,
                        color: flag.color
                    }
                })
            };
        })),
        xAxis: {
            events: {
                afterSetExtremes: function(e) {
                    mz.loadChartData(Math.round(e.min), Math.round(e.max));
                }
            },
            crosshair: {
                snap: false
            },
            // 4 hours
            minRange: 4 * 3600 * 1000,
            floor: dateFrom,
            ceiling: dateTo,
            ordinal: false
        },
        yAxis: {
            floor: 1,
            ceiling: mz.mapDataArray.length + 1,
            tickInterval: 1,
            labels: {
                formatter: function () {
                    var value = this.value;
                    var currentMap;
                    for (var i = 0; i < mz.mapDataArray.length; i++) {
                        currentMap = mz.mapDataArray[i];
                        if (value >= currentMap.inverseOrder && value < currentMap.inverseOrder + 1) {
                            return currentMap.shortName;
                        }
                    }
                    return "??";
                }
            }
        }
    });

    Highcharts.chart("stat-container", {
        chart: {
            type: "column",
            events: {
                load: function () {
                    mz.statChart = this;
                    mz.checkChartReady();
                }
            }
        },
        credits: {
            enabled: false
        },
        exporting: {
            buttons: {
                contextButton: {
                    menuItems: [{
                        text: "Download raw data (CSV)",
                        onclick: function () {
                            mz.downloadStatCsv();
                        }
                    }].concat(Highcharts.getOptions().exporting.buttons.contextButton.menuItems)
                }
            }
        },
        plotOptions: {
            column: {
                stacking: "normal",
                dataLabels: {
                    enabled: true
                }
            }
        },
        series: ["Splat Zones", "Rainmaker", "Tower Control", "Turf War"].map(function (x, i) {
            return { name: x, data: [], visible: i !== 3 };
        }),
        title: {
            text: "Map statistics",
            style: titleStyle
        },
        xAxis: {
            categories: mz.mapDataArray.map(function(mapData) {
                return mapData.name;
            })
        },
        yAxis: {
            title: {
                text: "Count"
            }
        }
    });
});
