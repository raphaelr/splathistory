﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using NPoco;
using SplatHistory.Helper;
using SplatHistory.Model;
using SplatHistory.Model.ViewModels;

namespace SplatHistory.Controllers
{
    public class MaintenanceController : Controller
    {
        private HttpClient _httpClient;
        private readonly IDatabase _database;

        public MaintenanceController(IDatabase database, NonRedirectingHttpClient httpClient)
        {
            _httpClient = httpClient.Value;
            _database = database;
        }

        [HttpGet]
        public async Task<IActionResult> GetRotation()
        {
            return Json(await GetRotationsIntern());
        }

        [HttpPost]
        public async Task ImportRotation()
        {
            var rotations = await GetRotationsIntern();

            if (rotations.Count > 0)
            using (var transaction = _database.GetTransaction())
            {
                var keys = rotations.Select(x => x.DateFrom).ToList();
                var sql = Sql.Builder
                    .Select("date_from")
                    .From("rotation")
                    .Where("date_from in (@keys)", new { keys });
                var existing = await _database.FetchAsync<DateTime>(sql);
                foreach (var toSave in rotations)
                {
                    if (!existing.Any(x => x == toSave.DateFrom))
                    {
                        // TODO: InsertAsync can't be used becasue the PostgreSQL database type has a buggy implementation for AutoIncrement
                        _database.Insert(toSave);
                    }
                }

                transaction.Complete();
            }
        }

        private async Task<List<Rotation>> GetRotationsIntern()
        {
            using (var resp = await _httpClient.GetAsync("https://splatoon.ink/schedule.json"))
            {
                var json = JObject.Parse(await resp.Content.ReadAsStringAsync());
                var rotations = new List<Rotation>();
                if (!json.Value<bool>("splatfest"))
                {
                    rotations.AddRange(json["schedule"].Select(ParseRotation));
                }
                return rotations;
            }
        }

        private static Rotation ParseRotation(JToken json)
        {
            var rotation = new Rotation
            {
                DateFrom = RotationViewModel.UnixEpoch.AddMilliseconds(json.Value<long>("startTime")),
                DateTo = RotationViewModel.UnixEpoch.AddMilliseconds(json.Value<long>("endTime"))
            };

            var rankedModeName = json["ranked"].Value<string>("rulesEN");

            switch (rankedModeName)
            {
                case "Splat Zones":
                    rotation.RankedMode = RankedMode.SplatZones;
                    break;
                case "Rainmaker":
                    rotation.RankedMode = RankedMode.Rainmaker;
                    break;
                case "Tower Control":
                    rotation.RankedMode = RankedMode.TowerControl;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(rankedModeName), rankedModeName);
            }

            rotation.RankedMaps = ParseStages(json["ranked"]);
            rotation.RegularMaps = ParseStages(json["regular"]);

            return rotation;
        }

        private static GameMap[] ParseStages(JToken stages)
        {
            var maps = stages["maps"];
            return maps.Select(x => MapTranslator[x.Value<string>("nameEN")]).ToArray();
        }

        private static readonly ImmutableDictionary<string, GameMap> MapTranslator = ImmutableDictionary<string, GameMap>.Empty.AddRange(new[]
        {
            new KeyValuePair<string, GameMap>("Ancho-V Games", GameMap.AnchoV),
            new KeyValuePair<string, GameMap>("Arowana Mall", GameMap.Mall),
            new KeyValuePair<string, GameMap>("Blackbelly Skatepark", GameMap.Skatepark),
            new KeyValuePair<string, GameMap>("Bluefin Depot", GameMap.Depot),
            new KeyValuePair<string, GameMap>("Camp Triggerfish", GameMap.Camp),
            new KeyValuePair<string, GameMap>("Flounder Heights", GameMap.Flounder),
            new KeyValuePair<string, GameMap>("Hammerhead Bridge", GameMap.Bridge),
            new KeyValuePair<string, GameMap>("Kelp Dome", GameMap.Dome),
            new KeyValuePair<string, GameMap>("Mahi-Mahi Resort", GameMap.MahiMahi),
            new KeyValuePair<string, GameMap>("Moray Towers", GameMap.Towers),
            new KeyValuePair<string, GameMap>("Museum d'Alfonsino", GameMap.Museum),
            new KeyValuePair<string, GameMap>("Piranha Pit", GameMap.Pit),
            new KeyValuePair<string, GameMap>("Port Mackerel", GameMap.Port),
            new KeyValuePair<string, GameMap>("Saltspray Rig", GameMap.Saltspray),
            new KeyValuePair<string, GameMap>("Urchin Underpass", GameMap.Underpass),
            new KeyValuePair<string, GameMap>("Walleye Warehouse", GameMap.Warehouse)
        });
    }
}