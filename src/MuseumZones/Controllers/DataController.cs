﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NPoco;
using SplatHistory.Model;
using SplatHistory.Model.ViewModels;

namespace SplatHistory.Controllers
{
    public class DataController : Controller
    {
        private readonly IDatabase _database;

        public DataController(IDatabase database)
        {
            _database = database;
        }

        public async Task<IActionResult> History(long ticksFrom, long ticksTo)
        {
            var dateFrom = RotationViewModel.UnixEpoch.AddMilliseconds(ticksFrom);
            var dateTo = RotationViewModel.UnixEpoch.AddMilliseconds(ticksTo).AddDays(1);

            var sql = new Sql()
                .Where("date_from >= @0 and date_from < @1", dateFrom, dateTo)
                .OrderBy("date_from");

            var rotations = await _database.FetchAsync<Rotation>(sql);
            var view = rotations.Select(x => new RotationViewModel(x)).ToList();

            return Ok(view);
        }
    }
}