﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using NPoco;
using SplatHistory.Model;
using SplatHistory.Model.ViewModels;

namespace SplatHistory.Controllers
{
    public class HomeController : Controller
    {
        private readonly IDatabase _database;

        public HomeController(IDatabase database)
        {
            _database = database;
        }

        public async Task<IActionResult> Index()
        {
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
            };
            var flags = await _database.FetchAsync<Flag>(Sql.Builder.OrderBy("date_event"));
            var staticData = new
            {
                MapData = GameMapData.Data.Values,
                Flags = flags.Select(x => new FlagViewModel(x))
            };
            var staticDataJson = JsonConvert.SerializeObject(staticData, Formatting.None, settings);
            ViewData["StaticData"] = staticDataJson;

            return View();
        }

        public IActionResult Status404()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
