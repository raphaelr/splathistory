﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;

namespace SplatHistory
{
    public class RewriteReasonMiddleware
    {
        private readonly RequestDelegate _next;

        public RewriteReasonMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public Task Invoke(HttpContext context)
        {
            context.Response.OnStarting(() =>
            {
                var response = context.Features.Get<IHttpResponseFeature>();
                if (response.StatusCode == 200)
                {
                    response.ReasonPhrase = "Stay Fresh";
                }
                return Task.CompletedTask;
            });
            return _next(context);
        }
    }
}