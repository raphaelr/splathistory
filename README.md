# Overview

SplatHistory is a web application for tracking, browsing and analyzing past
map rotation data. Although originally developed for Splatoon it shouldn't be
hard to modify the project to support a different game.


# Installation

To get started you need to install [.NET Core](https://www.microsoft.com/net/core)
for your system. You will also need a [PostgreSQL](https://postgresql.org)
database server to store the rotation data. Currently only PostgreSQL is
supported due to the use of array types.

Afterwards, load the schema from the "db_schema.sql" file into a new database
(the schema assumes the user "splathistory" exists and will grant all permissions
to that user).

Use the "dotnet publish" command to compile the application. As an example:

    cd src/MuseumZones
    dotnet publish -o <wherever the binaries should go>

You can then run the application using a command like

    env \
      'Logging__LogLevel__Default=Warning' \
      'ASPNETCORE_SERVER.URLS=http://localhost:1111' \
	  'ConnectionStrings__main=Server=localhost;Database=splathistory;User Id=splathistory;Password=secret' \
	  ~/dotnet/dotnet MuseumZones.dll`

You need to set up a job to periodically import the map rotation from [splatoon.ink](https://splatoon.ink).
For example, when using cron:

    22 *  *   *   *     curl -X POST --data "" http://localhost:1111/maintenance/importrotation

Set up your front-end server to forbid accessing the "/maintenance" path from the
internet. Afterwards, you should be good to go.

# License

Copyright (c) 2016 Raphael Robatsch

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
